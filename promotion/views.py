# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.contrib.auth import login as auth_login
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User

from members.models import Member
from members.forms import AuthenticationForm
from members.forms import RegistrationForm

# At this point, user is a User object that has already been saved
# to the database. You can continue to change its attributes
# if you want to change other fields.

def index(request):
    authentication_form = AuthenticationForm(request, prefix='authentication')
    registration_form = RegistrationForm(prefix='registration')

    if request.method == "POST":
        user = None

        # Connexion
        if "login" in request.POST:
            authentication_form = AuthenticationForm(request, data=request.POST, prefix='authentication')
            if authentication_form.is_valid():
                user = authentication_form.get_user()

                if user is not None:
                    auth_login(request, authentication_form.get_user())
                    return HttpResponseRedirect("/dashboard/index/")
        # Inscription
        elif "register" in request.POST:
            registration_form = RegistrationForm(data=request.POST, prefix='registration')

            if registration_form.is_valid():
                data = registration_form.cleaned_data

                username  = data['username']
                email     = data['email']
                password1 = data['password']
                password2 = data['password2']

                # Vérification de l'égalité des mots de passe
                if username and password1 == password2:
                    user = User.objects.create_user(username, email, password1)
                    user.save()

                    member = Member()
                    member.user = user
                    member.save()

                    return HttpResponseRedirect("/")

    # Vue
    context = {
        'authentication_form': authentication_form,
        'registration_form': registration_form,
    }

    return render(request, "promotion/index.html", context)

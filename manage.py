#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    # En local, l'application ne fonctionnait qu'avec l'ajout de cette ligne
    # sys.path.append("/home/Pi/workspace/casir_messenger/casir_messenger")

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "casir_messenger.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)

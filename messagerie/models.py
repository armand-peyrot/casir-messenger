from django.db import models

from members.models import Member

# Create your models here.
class Message(models.Model):
    content  = models.TextField()
    date     = models.DateTimeField(auto_now=True)
    readed   = models.BooleanField(default=False)
    file     = models.ImageField(upload_to="images/", default="images/no/no.jpg")
    duration = models.PositiveSmallIntegerField(choices=[(1, "1 seconde"), (3, "3 secondes"), (10, "10 secondes")])
    sender   = models.ForeignKey(Member, blank=False, null=True, related_name="message_sender")
    receiver = models.ForeignKey(Member, blank=False, null=True, related_name="message_receiver")

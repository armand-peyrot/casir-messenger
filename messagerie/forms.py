from django import forms
from members.models import Member

class SendMessageForm(forms.Form):
    recipient = forms.ChoiceField(label="Destinataire",
                                 choices=[])

    content = forms.CharField(label="Message",
                              widget=forms.Textarea(attrs={"class": "form-control", "placeholder": "Contenu du message"}))

    file = forms.ImageField(label="Fichier")

    duration = forms.ChoiceField(label="Duree",
                                 choices=[(1, "1 seconde"), (3, "3 secondes"), (10, "10 secondes")],
                                 initial=3)

    def __init__(self, friends):
        super(SendMessageForm, self).__init__()
        self.fields['recipient'].choices = [(friend.id, friend.user.username) for friend in friends]

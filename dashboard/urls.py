from django.conf.urls import patterns
from django.conf.urls import url

urlpatterns = patterns('dashboard.views',
                       url(r'index/$', 'index', name="index"),
                       url('^read/(?P<id_message>\d+)/$', 'read', name="read"),
                       url('^remove-friend/(?P<username>[a-zA-Z0-9_-]+)/$', 'remove_friend', name="remove_friend"))

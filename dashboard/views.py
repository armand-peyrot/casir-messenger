# -*- coding: utf-8 -*-

import json

from django.shortcuts import render
from django.http import HttpResponse
from django.db.models.query import QuerySet
from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger
from django.http import HttpResponseRedirect

from members.models import Member
from messagerie.models import Message
from messagerie.forms import SendMessageForm

def index(request):
    # Récuperation de l'utilisateur et des messages reçus
    user     = request.user
    member   = Member.objects.get(user=user)
    messages = Message.objects.filter(receiver=member).order_by("-date")

    # Récupération des messages d'un expéditeur particulier (si défini)
    sender = request.GET.get("sender")

    if sender:
        messages = messages.filter(sender__user__username=sender)

    # Pagination des résultats
    paginator = Paginator(messages, 5)

    page = request.GET.get("page")
    list_messages = []

    try:
        list_messages = paginator.page(page)
    except PageNotAnInteger:
        list_messages = paginator.page(1)
    except EmptyPage:
        list_messages = paginator.page(paginator.num_pages)

    # Récupération de la liste des amis
    if member.friends.count() == 0:
        friends = 0
    else:
        friends = member.friends.all()

    # Récupération des demandes d'amitié effectuées envers l'utilisateur
    if member.friend_requests.count() == 0:
        friend_requests = 0
    else:
        friend_requests = member.friend_requests.all()

    # Récupération des utilisateurs effectuées par l'utilisateur
    friend_requests_to = Member.objects.filter(friend_requests=member)

    # Exclusion des utilisateurs envers lesquels on a déjà effectué une demande
    no_friends = Member.objects.all().exclude(id__in=friend_requests_to)

    # Exclusion des amis
    if isinstance(friends, QuerySet):
        no_friends = no_friends.exclude(id__in=friends)

    # Exclusion des utilisateurs qui ont fait une demande
    if isinstance(friend_requests, QuerySet):
        no_friends = no_friends.exclude(id__in=friend_requests)

    # Exclusion de l'utilisateur lui-même
    no_friends = no_friends.exclude(id=member.id)

    # no_friends contient ainsi la liste des utilisateurs avec lesquels l'utilisateur
    # n'a AUCUNE relation (ni amitié et ni demande)

    if not friends:
        friends = []

    if not friend_requests:
        friend_requests = []

    send_msg_form = SendMessageForm(friends)

    if request.method == "POST":
        # Effecuter des demandes d'amitié
        if "members" in request.POST:
            for memberRequest in request.POST.getlist("members"):
                newFriend = Member.objects.get(id=memberRequest)
                newFriend.friend_requests.add(member)

            return HttpResponseRedirect("/dashboard/index/")

        # Acceptation des demandes d'amis
        if "action" in request.POST and "Accepter la/les demandes" in request.POST.get("action"):
            for memberChoice in request.POST.getlist("membersChoice"):
                newFriend = Member.objects.get(id=memberChoice)
                member.friend_requests.remove(memberChoice)
                newFriend.friends.add(member)

            return HttpResponseRedirect("/dashboard/index/")

        # Refus des demandes d'amis
        if "action" in request.POST and "Refuser la/les demandes" in request.POST.get("action"):
            for memberChoice in request.POST.getlist("membersChoice"):
                newFriend = Member.objects.get(id=memberChoice)
                member.friend_requests.remove(memberChoice)

            return HttpResponseRedirect("/dashboard/index/")

        # Envoyer un message
        if "send_msg" in request.POST:
            duration  = request.POST['duration']
            recipient = request.POST['recipient']
            content   = request.POST['content']

            new_message = Message()
            new_message.duration = duration
            new_message.receiver = Member.objects.get(id=recipient)
            new_message.sender   = member
            new_message.content  = content

            if "file" in request.FILES:
                new_message.file = request.FILES['file']

            new_message.save()

            return HttpResponseRedirect("/dashboard/index/")

    # Vue
    context = {
        'list_messages': list_messages,
        'friends': friends,
        'friend_requests': friend_requests,
        'no_friends': no_friends,
        'send_msg_form': send_msg_form,
        'paginator': paginator,
        'sender': sender,
        'current_path': request.get_full_path()
    }

    return render(request, "dashboard/index.html", context)

def read(request, id_message):
    # Récupération du message
    message = Message.objects.get(id=id_message)

    # Déjà lu : erreur 500
    if message.readed:
        return False

    # Passage en "lu"
    message.readed = True
    message.save()

    # Données relatives au message
    data = {
        'image': message.file.url,
        'content': message.content,
        'duration': message.duration,
        'sender_username': message.sender.user.username
    }

    # Envoie des données au format json
    return HttpResponse(json.dumps(data))

def remove_friend(request, username):
    user   = request.user
    member = Member.objects.get(user=user)
    friend = Member.objects.get(user__username=username)

    # Suppression de la relation d'amitié
    member.friends.remove(friend)

    return HttpResponseRedirect("/dashboard/index/")

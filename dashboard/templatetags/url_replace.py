from django import template

register = template.Library()

@register.simple_tag
def url_replace(path, sender, page):
    ret = '?page=' + str(page)

    if sender:
        ret += '&sender=' + str(sender)

    return ret

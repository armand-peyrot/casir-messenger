from django.conf.urls import patterns
from django.conf.urls import include
from django.conf.urls import url
from django.contrib import admin
from django.views.generic import TemplateView
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static

urlpatterns = patterns('',
   url(r'^admin/', include(admin.site.urls)),
   url(r'^403/$', TemplateView.as_view(template_name="403.html")),
   url(r'^404/$', TemplateView.as_view(template_name="404.html")),
   url(r'^500/$', TemplateView.as_view(template_name="500.html")),
   url(r'^503/$', TemplateView.as_view(template_name="503.html")),
   url(r'^membre/', include("members.urls", namespace="members")),
   url(r'^dashboard/', include("dashboard.urls")),
   url(r'', include("promotion.urls", namespace="promotion")),

   # Example template
   #url(r'^dashboard/index/$', TemplateView.as_view(template_name="examples/dashboard/index.html")),
   ) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # Add the Debug Toolbar's URLs to the project's URLconf
    import debug_toolbar
    urlpatterns += patterns('', url(r'^__debug__/', include(debug_toolbar.urls)),)

    # In DEBUG mode, serve media files through Django.
    urlpatterns += staticfiles_urlpatterns()
    # Remove leading and trailing slashes so the regex matches.
    media_url = settings.MEDIA_URL.lstrip('/').rstrip('/')
    #urlpatterns += patterns('', url(r'^%s/(?P<path>.*)$' % media_url, 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),)

from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Member(models.Model):
    user            = models.OneToOneField(User)
    friends         = models.ManyToManyField("self", blank=True, related_name="list_friends", symmetrical=True)
    friend_requests = models.ManyToManyField("self", blank=True, related_name="list_friend_requests", symmetrical=False)

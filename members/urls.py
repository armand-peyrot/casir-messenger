from django.conf.urls import url
from django.conf.urls import patterns

urlpatterns = patterns('',
                       url(r'^se-deconnecter/$', 'django.contrib.auth.views.logout', {'next_page': "/"}, name="logout"),
                    )

from django import forms
from django.contrib.auth.forms import AuthenticationForm as DjangoAuthenticationForm

class RegistrationForm(forms.Form):
    username = forms.CharField(label="Nom d'utilisateur",
                                max_length=254,
                                widget=forms.TextInput(attrs={"class": "form-control", 'placeholder': 'Nom d\'utilisateur'}))

    email = forms.EmailField(label="Courriel",
                                widget=forms.EmailInput(attrs={"class": "form-control", 'placeholder': 'Courriel'}))

    password = forms.CharField(label="Mot de passe",
                                widget=forms.PasswordInput(attrs={"class": "form-control", 'placeholder': 'Mot de passe'}))

    password2 = forms.CharField(label="Confirmation mot de passe",
                                widget=forms.PasswordInput(attrs={"class": "form-control", 'placeholder': 'Confirmation mot de passe'}))

class AuthenticationForm(DjangoAuthenticationForm):
    username = forms.CharField(label="Nom d'utilisateur",
                               max_length=254,
                               widget=forms.TextInput(attrs={"class": "form-control", 'placeholder': 'Nom d\'utilisateur'}))

    password = forms.CharField(label="Mot de passe",
                               widget=forms.PasswordInput(attrs={"class": "form-control", 'placeholder': 'Mot de passe'}))
